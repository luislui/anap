package com.example.lui_project;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lui_project.base.BaseActivity;
import com.example.lui_project.utils.Constant;
import com.example.lui_project.utils.GetBMIValuesHelper;
import com.example.lui_project.utils.SaveKeyValues;
import mrkj.library.wheelview.scalerulerview.ScaleRulerView;

import java.util.Calendar;
import java.util.Map;


/**
 * Make a plan --> set related information
 */
public class PlanningActivity extends BaseActivity implements View.OnFocusChangeListener, View.OnClickListener{
    private static final int SET_START_DATE = 0;//Set start time
    private static final int SET_STOP_DATE = 1;//Set end time
    // 功能
    private DatePickerDialog datePickerDialog;//Create time selector
    private GetBMIValuesHelper getBMIValuesHelper;//Get body information
    //控件
    private TextView setStartTime,setStopTime;//Set the start and end time
    private TextView show_plan_weight;//Display expected weight
    private ScaleRulerView setPlanWeight;//Set target weight
    private Button finish;//Finish button
    private TextView capion;//Standard weight range prompt
    private TextView hint;//hint
    // value
    private boolean isSetStart;//Difference setting
    private int nowYear;//Current year
    private int nowMonth;//current month
    private int nowDate;//current day
    private Double min_normal_weight;//standard body weight minimum
    private Double max_normal_weight;//Maximum standard weight
    private int now_weight;//current weight
    private int start_year;//starting year
    private int start_month;//start month
    private int start_date;//start day
    private int stop_year;//end year
    private int stop_month;//end month
    private int stop_date;//end day
    private int plan_want_weight;//target weight
    /* Set the title */
    @Override
    protected void setActivityTitle() {
        initTitle();//init title
        setTitle(getString(R.string.target));//set title
        setTitleTextColor(getResources().getColor(R.color.black));//set text color
    }

    /**
     * init layer
     */
    @Override
    protected void getLayoutToView() {
        setContentView(R.layout.activity_planning);
    }

    /**
     * init related variable
     */
    @Override
    protected void initValues() {
        //Set default load find page
        SaveKeyValues.putIntValues("launch_which_fragment", Constant.MAKE_PLAN);
        getNowDate();
        int height = SaveKeyValues.getIntValues("height",0);
        int weight = SaveKeyValues.getIntValues("weight",0);
        Log.e("身高體重值","身高："+height + "\t\t體重："+weight);
        getBMIValuesHelper = new GetBMIValuesHelper();
        Map<String,Double> map = getBMIValuesHelper.getNormalWeightRange(height);
        min_normal_weight = map.get("min");
        max_normal_weight = map.get("max");
        now_weight = weight;
    }

    /**
     * Current date
     */
    private void getNowDate(){
        Calendar calendar = Calendar.getInstance();
        nowYear = calendar.get(Calendar.YEAR);
        nowMonth = calendar.get(Calendar.MONTH);
        nowDate = calendar.get(Calendar.DAY_OF_MONTH);
    }
    /**
     * init control
     */
    @Override
    protected void initViews() {
        hint = (TextView) findViewById(R.id.change_txt);
        setStartTime = (TextView) findViewById(R.id.plan_start_time);
        setStopTime = (TextView) findViewById(R.id.plan_stop_time);
        setPlanWeight = (ScaleRulerView) findViewById(R.id.plan_input_weight);
        show_plan_weight = (TextView) findViewById(R.id.plan_show_weight);
        capion = (TextView) findViewById(R.id.show_normal_weight_range);
        finish = (Button) findViewById(R.id.finish);

    }

    /**
     * Set the ruler view
     */
    private ScaleRulerView.OnValueChangeListener set_plan_weight_listener = new ScaleRulerView.OnValueChangeListener() {
        @Override
        public void onValueChange(float value) {
            show_plan_weight.setText((int)value + getString(R.string.kg));
            plan_want_weight = (int) value;
            if ((int)value != now_weight){
                hint.setText(getString(R.string.plan_weight));
            }
        }
    };

    /**
     * monitor
     */
    @Override
    protected void setViewsListener() {
        setStartTime.setOnFocusChangeListener(this);
        setStopTime.setOnFocusChangeListener(this);
        setStartTime.setOnClickListener(this);
        setStopTime.setOnClickListener(this);
        finish.setOnClickListener(this);
        setPlanWeight.setValueChangeListener(set_plan_weight_listener);
    }

    /**
     * set function
     */
    @Override
    protected void setViewsFunction() {
        setStartTime.setClickable(true);
        setStartTime.setFocusableInTouchMode(true);
        setStopTime.setClickable(true);
        setStopTime.setFocusableInTouchMode(true);
        capion.setText(getString(R.string.normal_weight_range) + min_normal_weight + "~" + max_normal_weight + getString(R.string.kg));
        //target height selector
        setPlanWeight.initViewParam(now_weight, 130, 30);
        //create time selector
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                if (isSetStart){//set start time
                    start_year = year;
                    start_month = monthOfYear + 1;
                    start_date = dayOfMonth;
                    setStartTime.setText(year+getString(R.string.year)+(monthOfYear+1)+getString(R.string.month)+dayOfMonth+getString(R.string.day));
                }else {//set end time
                    stop_year = year;
                    stop_month = monthOfYear + 1;
                    stop_date = dayOfMonth;
                    setStopTime.setText(year+getString(R.string.year)+(monthOfYear+1)+getString(R.string.month)+dayOfMonth+getString(R.string.day));
                }
            }
        },nowYear,nowMonth,nowDate);
    }

    /**
     * focus
     * @param v
     * @param hasFocus
     */
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()){
            case R.id.plan_start_time://Plan start time
                if (hasFocus){
                    isSetStart = true;
                    showDateDialog(SET_START_DATE);
                }
                break;
            case R.id.plan_stop_time://Project end time
                if (hasFocus){
                    isSetStart = false;
                    showDateDialog(SET_STOP_DATE);
                }
                break;
            default:
                break;
        }
    }

    /**
     * Display selection date and time popup
     * @param type
     */
    private void showDateDialog(int type){

        if (type == SET_START_DATE){
            datePickerDialog.setTitle(getString(R.string.set_start_time));
            datePickerDialog.show();
        }else {
            datePickerDialog.setTitle(getString(R.string.set_stop_time));
            datePickerDialog.show();
        }
    }
    /**
     * click event
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.plan_start_time:
                showDateDialog(SET_START_DATE);
                break;
            case R.id.plan_stop_time:
                showDateDialog(SET_STOP_DATE);
                break;
            case R.id.finish://完成
                String start_dates = setStartTime.getText().toString();
                String stop_dates = setStopTime.getText().toString();

                Log.d("start", start_dates);
                Log.d("end", stop_dates);
                if (plan_want_weight < min_normal_weight){
                    Toast.makeText(this,getString(R.string.welceom_plan_error1), Toast.LENGTH_SHORT).show();
                }else if(plan_want_weight <= max_normal_weight){
                    if ((!getString(R.string.set_start_time).equals(start_dates))&&(!getString(R.string.set_stop_time).equals(stop_dates))){
                       if ((start_year <= stop_year) && (start_month <= stop_month)){
                           if (start_month == stop_month) {
                                if (start_date > stop_date)
                                {
                                    Toast.makeText(this,getString(R.string.welceom_plan_error2), Toast.LENGTH_SHORT).show();
                                    break;
                                }
                           }
                           saveCustomSettingValues(start_dates,stop_dates);
                           Intent intent = new Intent(this,FunctionActivity.class);
                           startActivity(intent);
                           finish();

                       }else {
                           Toast.makeText(this,getString(R.string.welceom_plan_error2), Toast.LENGTH_SHORT).show();
                       }
                    }else {
                        Toast.makeText(this,getString(R.string.welceom_plan_error3), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(this,getString(R.string.welceom_plan_error4), Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    /**
     * Store user related information
    *, storage start date, year, month and day
     *       // 2, storage end date, year, month and day
      *      // 3, storage weight range value standard minimum and standard maximum
      *      // 4, store the target weight
     * @param start_dates
     * @param stop_dates
     */

    private void saveCustomSettingValues(String start_dates, String stop_dates){
        //1.
        SaveKeyValues.putStringValues("plan_start_date",start_dates);
        SaveKeyValues.putIntValues("plan_start_year", start_year);
        SaveKeyValues.putIntValues("plan_start_month" , start_month);
        SaveKeyValues.putIntValues("plan_start_day" , start_date);
        //2.
        SaveKeyValues.putStringValues("plan_stop_date",stop_dates);
        SaveKeyValues.putIntValues("plan_stop_year", stop_year);
        SaveKeyValues.putIntValues("plan_stop_month" , stop_month);
        SaveKeyValues.putIntValues("plan_stop_day", stop_date);
        //3.
        SaveKeyValues.putFloatValues("plan_min_normal_weight_values",(float)((double)min_normal_weight));
        SaveKeyValues.putFloatValues("plan_max_normal_weight_values",(float)((double)max_normal_weight));
        //4.
        SaveKeyValues.putIntValues("plan_want_weight_values",plan_want_weight);
    }
    /**
     * hide back
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            return false;
        }
        return false;
    }
}
