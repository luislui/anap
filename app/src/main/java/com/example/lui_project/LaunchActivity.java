package com.example.lui_project;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.WindowManager;

import com.example.lui_project.utils.Constant;
import com.example.lui_project.utils.SaveKeyValues;


/**
 * welcome page
 */
public class LaunchActivity extends AppCompatActivity {
    private boolean isFirst;//Check that the microphone opens the app for the first time.
    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == 1) {
                if (isFirst){
                    startActivity(new Intent(LaunchActivity.this, MainActivity.class));
                }else {
                    startActivity(new Intent(LaunchActivity.this, FunctionActivity.class));
                }
                finish();
            }
            return false;
        }
    });
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //唔show status list
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        //Set the default loading sports homepage
        SaveKeyValues.putIntValues("launch_which_fragment", Constant.TURN_MAIN);
        //Determine if it is the first time to start
        int count = SaveKeyValues.getIntValues("count" , 0);
        isFirst = (count == 0)? true : false;
        handler.sendEmptyMessageDelayed(1, 3000);
    }
    /**
     * Hide back key
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            return false;
        }
        return false;
    }
}
