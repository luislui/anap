package com.example.lui_project;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.example.lui_project.base.BaseActivity;
import com.example.lui_project.entity.FoodMessage;
import com.example.lui_project.entity.FoodType;
import com.example.lui_project.utils.DBHelper;

public class FoodHotListActivity extends BaseActivity {
    private int sign= - 1 ; //Control list expansion
    private String[] food_type_array;//Food type array
    private List<FoodType> food_list;//Data collection
    private ExpandableListView data_list;//Collapse listview
    private Bitmap[] bitmaps;//Image resource
    private int[] ids;//Image resource ID array
    int counts  = 0;
    /**
     * set title
     */
    @Override
    protected void setActivityTitle() {
        initTitle();
        setTitle(this.getString(R.string.FoodhotTitle), this);
        setMyBackGround(R.color.watm_background_gray);
        setTitleTextColor(R.color.theme_blue_two);
        setTitleLeftImage(R.mipmap.sport_back_blue);
    }

    /**
     *Setting the interface layout
     */
    @Override
    protected void getLayoutToView() {
        setContentView(R.layout.activity_food_hot_list);

    }

    /**
     * init
     */
    @Override
    protected void initValues() {
        ids = new int[]{R.mipmap.beantype, R.mipmap.vegetablestype,
        R.mipmap.fruitstype, R.mipmap.meattype, R.mipmap.eggtype,
        R.mipmap.aquatictype, R.mipmap.milktype, R.mipmap.beveragestype,
        R.mipmap.bacterialalgaetype, R.mipmap.fattype};
        bitmaps = new Bitmap[ids.length];
        for (int i = 0;i < ids.length ; i++){
            bitmaps[i] = BitmapFactory.decodeResource(getResources(),ids[i]);
        }
        food_type_array = new String[]{this.getString(R.string.Foodhot_food_type_array1),
                this.getString(R.string.Foodhot_food_type_array2), this.getString(R.string.Foodhot_food_type_array3), this.getString(R.string.Foodhot_food_type_array4),
                this.getString(R.string.Foodhot_food_type_array4), this.getString(R.string.Foodhot_food_type_array6), this.getString(R.string.Foodhot_food_type_array7),
                this.getString(R.string.Foodhot_food_type_array8), this.getString(R.string.Foodhot_food_type_array9), this.getString(R.string.Foodhot_food_type_array10)};
        food_list = new ArrayList<>();
        //struct data scores

        Locale locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = FoodHotListActivity.this.getResources().getConfiguration().getLocales().get(0);
        } else {
            locale = FoodHotListActivity.this.getResources().getConfiguration().locale;
        }
        String language = locale.getLanguage();
//        Log.e("among of data", cursor.getCount() + "");
//        int j = 0;
        if (language ==  "en") {
            DBHelper dbHelper = new DBHelper();
            Cursor cursor = dbHelper.selectAllDataOfTable("hot_en");
            for (int i = 0; i < 10; i++) {
                FoodType foodType = null;
                List<FoodMessage> foods = null;
                counts = 1;
                while (cursor.moveToNext()) {
//                Log.e("Count", (++j) + "");
//                Log.e("counts", counts + "");
                    String name = cursor.getString(cursor.getColumnIndex("name"));
                    String hot = cursor.getString(cursor.getColumnIndex("hot"));
                    String type_name = cursor.getString(cursor.getColumnIndex("type_name"));
                    if (counts == 1) {
                        foodType = new FoodType();
                        foods = new ArrayList<>();
                        foodType.setFood_type(type_name);
//                    Log.e("type_name", type_name + "");
                    }
                    FoodMessage foodMessage = new FoodMessage();
                    foodMessage.setFood_name(name);
                    foodMessage.setHot(hot);
                    foods.add(foodMessage);
                    foodType.setFood_list(foods);
                    if (counts == 20) {
                        food_list.add(foodType);
                        break;
                    }
                    counts++;
                }
            }
            cursor.close();
        }
        else{
            DBHelper dbHelper = new DBHelper();
            Cursor cursor = dbHelper.selectAllDataOfTable("hot");
            for (int i = 0; i < 10; i++) {
                FoodType foodType = null;
                List<FoodMessage> foods = null;
                counts = 1;
                while (cursor.moveToNext()) {
//                Log.e("Count", (++j) + "");
//                Log.e("counts", counts + "");
                    String name = cursor.getString(cursor.getColumnIndex("name"));
                    String hot = cursor.getString(cursor.getColumnIndex("hot"));
                    String type_name = cursor.getString(cursor.getColumnIndex("type_name"));
                    if (counts == 1) {
                        foodType = new FoodType();
                        foods = new ArrayList<>();
                        foodType.setFood_type(type_name);
//                    Log.e("type_name", type_name + "");

                    }
                    FoodMessage foodMessage = new FoodMessage();
                    foodMessage.setFood_name(name);
                    foodMessage.setHot(hot);
                    foods.add(foodMessage);
                    foodType.setFood_list(foods);
                    if (counts == 20) {
                        food_list.add(foodType);
                        break;
                    }
                    counts++;
                }
            }
            cursor.close();
        }

        Log.e("data lengths", food_list.size() + "");
//        for (FoodType foodType : food_list) {
//            Log.e("food type", foodType.getFood_type() + "");
//            Log.e("food type",foodType.getFood_list().size() + "");
//        }
    }

    @Override
    protected void initViews() {
        data_list = (ExpandableListView) findViewById(R.id.food_list);
    }
    /**
     * Adapter
     */
    @Override
    protected void setViewsFunction() {
        MyFoodAdapter adapter = new MyFoodAdapter();
        data_list.setAdapter(adapter);
    }
    /**
     * Set click to expand one and the rest are collapsed
     */
    @Override
    protected void setViewsListener() {
        data_list.setOnGroupClickListener( new  ExpandableListView.OnGroupClickListener() {

            @Override
            public   boolean  onGroupClick(ExpandableListView parent, View v,
                                           int  groupPosition, long  id) {
                // TODO Auto-generated method stub
                if  (sign== - 1 ) {
                    // Expand selected group
                    data_list.expandGroup(groupPosition);
                    //Set the selected group to the top
                    data_list.setSelectedGroup(groupPosition);
                    sign= groupPosition;
                }  else   if  (sign== groupPosition) {
                    data_list.collapseGroup(sign);
                    sign= - 1 ;
                }  else  {
                    data_list.collapseGroup(sign);
                    //Expand selected group
                    data_list.expandGroup(groupPosition); // Set the selected group to the top of the system
                    data_list.setSelectedGroup(groupPosition);
                    sign= groupPosition;
                }
                return   true ;
            }
        });
    }




    /**
     * set Adapter
     */

    class MyFoodAdapter extends BaseExpandableListAdapter {

        //Number of Groups
        @Override
        public int getGroupCount() {
            return food_list.size();
        }
        //The number of Child in each group
        @Override
        public int getChildrenCount(int groupPosition) {
            return food_list.get(groupPosition).getFood_list().size();
        }
        //Get the group corresponding to the location
        @Override
        public Object getGroup(int groupPosition) {
            return food_list.get(groupPosition);
        }
        //Get Child in the corresponding position
        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return food_list.get(groupPosition).getFood_list().get(childPosition);
        }
        //Get the ID of the group corresponding to the location
        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }
        //Get the ID of the child in the corresponding position
        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }
        //Determine if the same ID points to the same object
        @Override
        public boolean hasStableIds() {
            return true;
        }
        //Get the view of the Group
        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            GroupViewHolder holder;
            if (convertView == null){
                holder = new GroupViewHolder();
                convertView = getLayoutInflater().inflate(R.layout.group_item , null);
                holder.image = (ImageView) convertView.findViewById(R.id.group_image);
                holder.title = (TextView) convertView.findViewById(R.id.group_title);
                convertView.setTag(holder);
            }else {
                holder = (GroupViewHolder) convertView.getTag();
            }
            Log.d("image","seting");
            holder.image.setImageBitmap(bitmaps[groupPosition]);
            holder.title.setText(food_type_array[groupPosition]);
            return convertView;
        }
        //獲取child的view
        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            ChildViewHolder holder;
            if (convertView == null){
                holder = new ChildViewHolder();
                convertView = getLayoutInflater().inflate(R.layout.child_item,null);
                holder.name = (TextView) convertView.findViewById(R.id.food_name);
                holder.hot = (TextView) convertView.findViewById(R.id.food_hot);
                convertView.setTag(holder);
            }else {
                holder = (ChildViewHolder) convertView.getTag();
            }
            FoodMessage food = food_list.get(groupPosition).getFood_list().get(childPosition);
            holder.name.setText(food.getFood_name());
            holder.hot.setText(food.getHot()+FoodHotListActivity.this.getString(R.string.Foodhot_hot));
            return convertView;
        }

        //Determine if child can be selected
        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }

    class GroupViewHolder{
        ImageView image;
        TextView title;
    }
    class ChildViewHolder{
        TextView name,hot;
    }
}
