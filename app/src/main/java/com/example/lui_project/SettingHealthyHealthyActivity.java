package com.example.lui_project;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Map;


import com.example.lui_project.db.DatasDao;
import com.example.lui_project.utils.Constant;
import com.example.lui_project.utils.DateUtils;

/**
 * Plus exercise plan interface
 */
public class SettingHealthyHealthyActivity extends AppCompatActivity implements TimePicker.OnTimeChangedListener, View.OnClickListener {

    private TextView back;
    private TextView title;//Type of exercise
    private int type;
    private String title_name;
    private TimePicker timePicker;//set time
    private int alarmhour;// hour
    private int alarmminute;//min
    private DatePickerDialog datePickerDialog;
    private Button start, stop;
    private int index;//Distinguish between start and end
    private int start_year,start_month,start_day,stop_year,stop_month,stop_day;
    private Button finish_setting;
    //save data
    private DatasDao datasDao;
    public boolean isToSave;

    public static String[] warm_up_exercise = new String[5];
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_healthy_healthy);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);// set布局填充activity界面

        datasDao = new DatasDao(this);
        Intent intent = getIntent();

        //back
        back = (TextView) findViewById(R.id.to_back);
        back.setOnClickListener(this);
        //type
        title = (TextView) findViewById(R.id.plan_type);
        type = intent.getIntExtra("type", 0);
        warm_up_exercise[0]=this.getString(R.string.warm_up_exercise0);
        warm_up_exercise[1]=this.getString(R.string.warm_up_exercise1);
        warm_up_exercise[2]=this.getString(R.string.warm_up_exercise2);
        warm_up_exercise[3]=this.getString(R.string.warm_up_exercise3);
        warm_up_exercise[4]=this.getString(R.string.warm_up_exercise4);
        title_name = warm_up_exercise[type];
        title.setText(title_name);
        //time
        timePicker = (TimePicker) findViewById(R.id.timePicker1);
        timePicker.setOnTimeChangedListener(this);
        //date
        Map<String, Object> timeMap = DateUtils.getDate();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                
                switch (index){
                    case 0://start
                        start_year = year;
                        start_month = monthOfYear + 1;
                        start_day = dayOfMonth;
                        start.setText(SettingHealthyHealthyActivity.this.getString(R.string.Setting_plan_start_point)+start_year + "-" + start_month + "-" + start_day);
                        break;
                    case 1://stop
                        stop_year = year;
                        stop_month = monthOfYear + 1;
                        stop_day = dayOfMonth;
                        stop.setText(SettingHealthyHealthyActivity.this.getString(R.string.Setting_plan_end_point)+stop_year + "-" + stop_month + "-" + stop_day);
                        break;
                    default:
                        break;

                }
            }
        }, (Integer) timeMap.get("year"), (Integer) timeMap.get("month") - 1, (Integer) timeMap.get("day"));
        start = (Button) findViewById(R.id.plan_start);
        stop = (Button) findViewById(R.id.plan_stop);
        start.setOnClickListener(this);
        stop.setOnClickListener(this);
        //complete
        finish_setting = (Button) findViewById(R.id.set_clock);
        finish_setting.setOnClickListener(this);
    }

    /**
     * click time
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.to_back://back
                finish();
                break;
            case R.id.plan_start://set start time
                index = 0;
                datePickerDialog.setTitle(SettingHealthyHealthyActivity.this.getString(R.string.Setting_plan_dialogstart_title));
                datePickerDialog.show();
                break;
            case R.id.plan_stop://set end time
                index = 1;
                datePickerDialog.setTitle(SettingHealthyHealthyActivity.this.getString(R.string.Setting_plan_dialogend_title));
                datePickerDialog.show();
                break;
            case R.id.set_clock://set 完成
                Log.e("分割","====================");
//                Log.e("分割","");
                int isAdd_24_hours = 0;
                long nowTime = DateUtils.getMillisecondValues((int) DateUtils.getDate().get("hour"), (int) DateUtils.getDate().get("minute"));
                //對要save的數值先做處理
                long wantSaveTime = DateUtils.getMillisecondValues(alarmhour,alarmminute);
                int selectID = 0;
//                Log.e("增加前", wantSaveTime + "");
                if (wantSaveTime <= nowTime ){
                    wantSaveTime += Constant.DAY_FOR_24_HOURS;
                    isAdd_24_hours = 1;
//                    Log.e("有無執行？","加上了24個小時");
                }
                //if no set end time
                if ( start_year != 0 && stop_year == 0){
                    stop_year = start_year;
                    stop_month = start_month;
                    stop_day = start_day;
                }
                //if no set start time
                if( start_year == 0 && stop_year != 0){
                    start_year = (int) DateUtils.getDate().get("year");
                    start_month = (int) DateUtils.getDate().get("month");
                    start_day = (int) DateUtils.getDate().get("day");
                }
                //No substantive planning time
                if(start_year == 0 && stop_year == 0){
                    start_year = (int) DateUtils.getDate().get("year");
                    start_month = (int) DateUtils.getDate().get("month");
                    start_day = (int) DateUtils.getDate().get("day");
                    stop_year = start_year;
                    stop_month = start_month;
                    stop_day = start_day;
                }
                //set的時間不規範

                if (DateUtils.getMillisecondValues(start_year,start_month,start_day) > DateUtils.getMillisecondValues(stop_year,stop_month,stop_day)){
                    Toast.makeText(this,SettingHealthyHealthyActivity.this.getString(R.string.Setting_plan_dialog_error1), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (DateUtils.getMillisecondValues((Integer) DateUtils.getDate().get("year"),(int) DateUtils.getDate().get("month"),(int) DateUtils.getDate().get("day")) > DateUtils.getMillisecondValues(stop_year,stop_month,stop_day)){
                    Toast.makeText(this,SettingHealthyHealthyActivity.this.getString(R.string.Setting_plan_dialog_error2), Toast.LENGTH_SHORT).show();
                    return;
                }
                contain_data(  type,  title_name,  start_year, start_month, start_day, stop_year, stop_month, stop_day,  nowTime ,  wantSaveTime,  isAdd_24_hours,  selectID );

                break;
            default:
                break;
        }
    }

    public Boolean contain_data( int type, String title_name, int start_year,int start_month,int start_day,int stop_year,int stop_month,int stop_day, Long nowTime , Long wantSaveTime, int isAdd_24_hours, int selectID )
    {
        final ContentValues values = new ContentValues();
        // 1。Deposit into sport type
        values.put("sport_type" , type);//1
        Log.d("sport_type",type+"");
        // 2。Save the sport type name
        values.put("sport_name" , title_name);//2
        Log.d("sport_name",title_name+"");
        //3。Deposit date
        values.put("start_year" , start_year);//3
        Log.d("start_year",start_year+"");
        values.put("start_month" , start_month);//4
        Log.d("start_month",start_month+"");
        values.put("start_day" , start_day);//5
        Log.d("start_day",start_day+"");
        // 4。Deposit end date
        values.put("stop_year" , stop_year);//6
        Log.d("stop_year",stop_year+"");
        values.put("stop_month" , stop_month);//7
        Log.d("stop_month",stop_month+"");
        values.put("stop_day" , stop_day);//8
        Log.d("stop_day",stop_day+"");
        //5。Deposit time set
        values.put("set_time", nowTime);//9
        Log.d("set_time",nowTime+"");
        //6。Deposit reminder time
        if (alarmminute == 0 && alarmhour == 0){
            alarmhour = (int) DateUtils.getDate().get("hour");
            alarmminute = (int) DateUtils.getDate().get("minute");
        }
        values.put("hint_time" , wantSaveTime);//10
        //Log.e（“想要設置的時間”，wantSaveTime +“”）;
        values.put("hint_str" , alarmhour + ":" + alarmminute );
        values.put("hint_hour",alarmhour);
        values.put("hint_minute",alarmminute);
        //7。Deposit order
        values.put("number_values" , 0);
        values.put("add_24_hour",isAdd_24_hours);
        //Make a judgment before inserting. If the reminder time you want to set already exists, prompt the user whether to overwrite the previous setup time.
        Cursor cursor = datasDao.selectColumn("plans", new String[]{"_id", "hint_time"});
        //If the value of the cursor is greater than 0, it means that there is data in the database's schedule.
        if (cursor.getCount() != 0){

            //Check if the data has this value already stored in the data table
            while (cursor.moveToNext()){

                int id = cursor.getInt(cursor.getColumnIndex("_id"));
                long alarmTime = cursor.getLong(cursor.getColumnIndex("hint_time"));
                // Log.e（“查詢出的ID”，id +“”）;
                //// Log.e（“查詢的時間”，alarmTime +“”）;
                //// Log.e（“比較”，wantSaveTime +“<--->”+ alarmTime）;

                if (wantSaveTime == alarmTime){//Two times equal, indicating that the time point has been occupied
                // Log.e（“是否進入判斷”，（wantSaveTime  -  alarmTime）+“是”）;
                    Log.d("wantSaveTime",wantSaveTime+"");
                    selectID = id;

                    Log.d("selectID",selectID+"");
                    break;
                }else{
                    // Log.e（“是否進入判斷”，（wantSaveTime  -  alarmTime）+“no”）;
                }
            }

                //Log.e（“執行完循環體”，“是”）;
                //                    cursor.close（）;
                //// Log.e（“查詢出的相同數據ID”，selectID +“”）;
            if (selectID != 0){
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                dialog.setTitle(SettingHealthyHealthyActivity.this.getString(R.string.Setting_plan_dialog_settingError_Title));
                dialog.setMessage(SettingHealthyHealthyActivity.this.getString(R.string.Setting_plan_dialog_settingError_Message));
                final int finalSelectID = selectID;
                dialog.setPositiveButton(SettingHealthyHealthyActivity.this.getString(R.string.Setting_plan_dialog_EnterButton), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        datasDao.deleteValue("plans","_id=?",new String[]{String.valueOf(finalSelectID)});
                        isToSave = true;
                        insertData(values);
                    }
                });
                dialog.setNegativeButton(SettingHealthyHealthyActivity.this.getString(R.string.Setting_plan_dialog_CancelButton), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        isToSave = false;
                        Toast.makeText(SettingHealthyHealthyActivity.this, SettingHealthyHealthyActivity.this.getString(R.string.Setting_plan_dialog_CancelMsg), Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.create();
                dialog.show();
            }else{
                isToSave = true;
                insertData(values);
            }
        }else {
            isToSave = true;
            insertData(values);
        }

        return isToSave;
    }


    private void insertData(ContentValues values){
        if (isToSave){
           // Insert data into the database
            long result = datasDao.insertValue("plans",values);
            if (result > 0){
                Toast.makeText(this,SettingHealthyHealthyActivity.this.getString(R.string.Setting_plan_settingSuccess_Msg), Toast.LENGTH_SHORT).show();
                setResult(RESULT_OK);
                finish();
            }else {
                Toast.makeText(this,SettingHealthyHealthyActivity.this.getString(R.string.Setting_plan_settingFalse_Msg), Toast.LENGTH_SHORT).show();
            }
        }
    }


    /**
     * 改時間
     *
     * @param view
     * @param hourOfDay
     * @param minute
     */
    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
//        Log.e("hourOfDay",hourOfDay + "時");
//        Log.e("minute", minute + " 分");
        alarmhour = hourOfDay;
        alarmminute = minute;
    }
}
