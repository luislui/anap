package com.example.lui_project;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.lui_project.base.BaseActivity;
import com.example.lui_project.circlebar.CircleImageView;
import com.example.lui_project.utils.DateUtils;
import com.example.lui_project.utils.SaveKeyValues;


import java.util.Map;


/**
 * Change user information
 */
public class CompileDetailsActivity extends BaseActivity implements View.OnClickListener{

    private CircleImageView head_image;//Display avatar
    // 2、change username
    private String nick_str;//user name
    private EditText change_nick;//change username
    // 3、Modify gender
    private RadioGroup change_gender;//更改性別
    String gender; //current gender
    private String sex_str;//sex
    // 4、Modify birthday
    private TextView change_birthDay;//modify
    private String date; //birdthday
    private int birth_year;
    private int birth_month; private int birth_day;//current date
    private int now_year ; private int now_month; private int now_day;
    //5、Modify height
    private EditText change_height;
    private int height;
    //6、modify weight
    private EditText change_weight;
    private int weight;
    //7、Modify step size
    private EditText change_length;
    private int length;
    //User age
    // Make sure to edit
    private Button change_OK_With_Save;
    //Determine save
    @Override
    protected void setActivityTitle() {
        initTitle();
        setTitle(getString(R.string.change_persona_information), this);
        setMyBackGround(R.color.watm_background_gray);
        setTitleTextColor(R.color.theme_blue_two);
        setTitleLeftImage(R.mipmap.sport_back_blue);
        setResult(RESULT_OK);
    }

    @Override
    protected void getLayoutToView() {
        setContentView(R.layout.activity_compile_details);
    }

    @Override
    protected void initValues() {
        nick_str = SaveKeyValues.getStringValues("nick","未填");
        sex_str = SaveKeyValues.getStringValues("gender","M");
        Log.d("getsex", sex_str+"");
        //Get today's date
        getTodayDate();
        birth_year = SaveKeyValues.getIntValues("birth_year",now_year);
        birth_month = SaveKeyValues.getIntValues("birth_month",now_month);
        birth_day = SaveKeyValues.getIntValues("birth_day",now_day);
        date = birth_year+"-"+birth_month+"-"+birth_day;

        height = SaveKeyValues.getIntValues("height",0);
        weight = SaveKeyValues.getIntValues("weight",0);
        length = SaveKeyValues.getIntValues("length",0);
    }

    /**
     * Get the date of the today
     */
    private void getTodayDate() {
        Map<String,Object> map = DateUtils.getDate();
        now_year = (int) map.get("year");
        now_month = (int) map.get("month");
        now_day = (int) map.get("day");
    }

    @Override
    protected void initViews() {

        head_image = (CircleImageView) findViewById(R.id.head_pic);
        //2、Rename
        change_nick = (EditText) findViewById(R.id.change_nick);
        //3、Change gender
        change_gender = (RadioGroup) findViewById(R.id.change_gender);

        //4、Change birthday
        change_birthDay = (TextView) findViewById(R.id.change_date);
        //Confirm exit
        change_OK_With_Save = (Button) findViewById(R.id.change_ok);

        change_height = (EditText) findViewById(R.id.change_height);
        change_weight = (EditText) findViewById(R.id.change_weight);
        change_length = (EditText) findViewById(R.id.change_length);
    }

    @Override
    protected void setViewsListener() {
        change_OK_With_Save.setOnClickListener(this);
        change_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                try {
                    hideKeyBoard();
                }
                catch (Exception e){
                    Log.d("keybroad","null");
                }
                switch (checkedId) {
                    case R.id.change_girl:
                        sex_str = getResources().getString(R.string.girl);
                        break;
                    case R.id.change_boy:
                        sex_str = getResources().getString(R.string.boy);;
                        break;
                    default:
                        sex_str = gender;
                        break;
                }
            }
        });
        change_birthDay.setOnClickListener(this);
    }

    @Override
    protected void setViewsFunction() {
        gender = SaveKeyValues.getStringValues("gender", "M");//Get image path
        // Set display and function
        Log.d("sex!!!",gender+"");
        if (gender == getResources().getString(R.string.boy))
        {
            Log.d("sex!!!","set boy");
            Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(),R.mipmap.welcome_boy_blue);
            head_image.setImageBitmap(bitmap);
            change_gender.check(R.id.change_boy);
        }
        else
        {
            Log.d("sex!!!","set girl");
            Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(),R.mipmap.welcome_girl_blue);
            head_image.setImageBitmap(bitmap);
            change_gender.check(R.id.change_girl);
        }
        change_birthDay.setText(date);
        change_nick.setHint(nick_str);
        change_nick.setHintTextColor(getResources().getColor(R.color.btn_gray));
        change_height.setHint(String.valueOf(height));
        change_height.setHintTextColor(getResources().getColor(R.color.btn_gray));
        change_length.setHint(String.valueOf(length));
        change_length.setHintTextColor(getResources().getColor(R.color.btn_gray));
        change_weight.setHint(String.valueOf(weight));
        change_weight.setHintTextColor(getResources().getColor(R.color.btn_gray));

    }


    /**
     * click event
     * * @param v
     */
    @Override
    public void onClick(View v) {
        try {
            hideKeyBoard();
        }
        catch (Exception e)
        {
            Log.d("keybroad","null");
        }
        switch (v.getId()){

            case R.id.change_date://Change date --> change age
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        birth_day = year;
                        birth_month = monthOfYear + 1;
                        birth_day = dayOfMonth;
                        date = birth_year+"-"+birth_month+"-"+birth_day;
                        change_birthDay.setText(date);
                    }
                },birth_year,birth_month - 1,birth_day);
                datePickerDialog.setTitle(getString(R.string.please_Set_birdthday));
                datePickerDialog.show();
                break;
            case R.id.change_ok:
             {
                 if(!"".equals(change_nick.getText().toString())) {
                     SaveKeyValues.putStringValues("nick", change_nick.getText().toString());//save nick name
                 }
                 SaveKeyValues.putStringValues("gender", sex_str);//save sex
                 Log.d("sex!!!!!!!!!1change", sex_str+"");
                 SaveKeyValues.putStringValues("birthday", birth_year + "年" + birth_month + "月" + birth_day + "日");//save birthday
                 SaveKeyValues.putIntValues("birth_year", birth_year);
                 SaveKeyValues.putIntValues("birth_month", birth_month);
                 SaveKeyValues.putIntValues("birth_day", birth_day);
                 SaveKeyValues.putIntValues("age", now_year - birth_year);//save age
                 if (!"".equals(change_height.getText().toString())){
                     SaveKeyValues.putIntValues("height", Integer.parseInt(change_height.getText().toString().trim()));//save height
                 }
                 if (!"".equals(change_length.getText().toString())){
                     SaveKeyValues.putIntValues("length", Integer.parseInt(change_length.getText().toString().trim()));//save step length
                 }
                 if (!"".equals(change_weight.getText().toString())){
                     SaveKeyValues.putIntValues("weight", Integer.parseInt(change_weight.getText().toString().trim()));//save weight
                 }
                 Log.d("update","start");
                 startActivity(new Intent(this, PlanningActivity.class));
                 finish();
             }
            default:
                break;
        }
    }

    /**
     * hide keybroad
     */
    private void  hideKeyBoard(){
        ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(CompileDetailsActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

}
