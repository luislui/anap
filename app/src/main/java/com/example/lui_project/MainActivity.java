package com.example.lui_project;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lui_project.base.BaseActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import com.example.lui_project.utils.SaveKeyValues;

import mrkj.library.wheelview.pickerView.PickerView;
import mrkj.library.wheelview.scalerulerview.ScaleRulerView;
import mrkj.library.wheelview.utils.DateViewHelper;
import mrkj.library.wheelview.adapter.NumericWheelAdapter;


/**
 * Improve the information interface
 * -->The main interface is FunctionActivity
 */
public class MainActivity extends BaseActivity implements View.OnClickListener, PickerView.onSelectListener, RadioGroup.OnCheckedChangeListener, DateViewHelper.OnResultMessageListener, View.OnFocusChangeListener{

    //TAG
    private static final String TAG = MainActivity.class.getSimpleName();
    //function
    private DateViewHelper dateViewHelper;//Calendar operation
    private LayoutInflater inflater;//Layout filler
    private boolean closeDataPicker;//Determine whether to show or hide the calendar
    private List<String> height_list;//Height collection
    private boolean closeHeightPicker;//Judge whether to show or hide the number selector
    private boolean nextShow;//Determine if the button is displayed
    // control
    private LinearLayout personal_information_page_one;//Improve the information 1/2 layout
    private RadioGroup group;//Gender selection
    private EditText input_nick;//Property nickname
    private TextView input_birthday,input_height; //Birthday, height
    private Button next_action;//next step
    private LinearLayout choose_date;//select date
    private LinearLayout choose_height;//select height
    private PickerView height_picker;//customer view
    private ImageView back;//Return to the previous step
    private LinearLayout personal_information_page_two;//Improve the material 2/2 layout
    private ScaleRulerView input_weight;//Choose weight
    private TextView show_weight;//Show selected weight
    private ScaleRulerView input_length;//Select step length
    private TextView show_length;//Show Selected step length
    private Button go_walk;//take a look the app without setting plan
    private Button go_make;//set a plan
    // /信息
    private String gender_str;//sex
    private String nick_str;//nickman
    private String birthday_str;//birthday
    private String height_str;//hright
    private int custom_age;//age
    private String weight_str;//weight
    private int weight;//weight value
    private String length_str;//step length
    private int length;//step length value

    private int year;
    private int month;
    private  int day;
    /**
     * init view
     */
    @Override
    protected void getLayoutToView() {
        setContentView(R.layout.activity_main);
    }
    /**
     * init title
     */
    @Override
    protected  void setActivityTitle(){
        initTitle();
        setTitle(getString(R.string.personal_information_one));
        setTitleTextColor(getResources().getColor(R.color.black));
        //Calendar c = Calendar.getInstance();
        //int norYear = c.get(1);
        //NumericWheelAdapter numericWheelAdapter1 = new NumericWheelAdapter(this, 1950, norYear);
        //numericWheelAdapter1.setLabel("year");

        //NumericWheelAdapter numericWheelAdapter2 = new NumericWheelAdapter(this, 1, 12, "%02d");
        //numericWheelAdapter2.setLabel("month");
        //NumericWheelAdapter numericWheelAdapter = new NumericWheelAdapter(this, 1, this.getDay(1560, 12), "%02d");
        //numericWheelAdapter.setLabel("day");


    }
    /**
     * Set the value and variable of init
     */
    @Override
    protected void initValues() {

        gender_str = getResources().getString(R.string.boy);
        initHeightData();
        nextShow = true;
    }
    private int getDay(int year, int month) {
        int day ;
        boolean flag = false;
        switch(year % 4) {
            case 0:
                flag = true;
                break;
            default:
                flag = false;
        }


        switch(month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                day = 31;
                break;
            case 2:
                day = flag ? 29 : 28;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
            default:
                day = 30;
        }
        return day;
    }
    /**
     * Init height collection
     */
    private void initHeightData(){
        //set 130cm to 210cm
        height_list = new ArrayList<>();
        for (int i = 130;i <= 210;i++){
            height_list.add(i+"");
        }
    }

    /**
     * init control
     */
    @Override
    protected void initViews() {

        //======================================= Init height collection 1/2 =======================================

        personal_information_page_one = (LinearLayout) findViewById(R.id.personal_information_page_one);
        group = (RadioGroup) findViewById(R.id.gender);
        input_nick = (EditText) findViewById(R.id.input_nick);
        input_birthday = (TextView) findViewById(R.id.input_birthday);
        input_height = (TextView) findViewById(R.id.input_height);
        next_action = (Button) findViewById(R.id.next);
        //date selector
        inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        choose_date=(LinearLayout) findViewById(R.id.choose_date);
        dateViewHelper = new DateViewHelper(this);
        //height selector
        choose_height = (LinearLayout) findViewById(R.id.choose_height);
        height_picker = (PickerView) findViewById(R.id.height_picker);


        //======================================= Init height collection 2/2 =======================================

        personal_information_page_two = (LinearLayout) findViewById(R.id.personal_information_page_two);
        input_weight = (ScaleRulerView) findViewById(R.id.input_weight);
        show_weight = (TextView) findViewById(R.id.show_weight);
        input_length = (ScaleRulerView) findViewById(R.id.input_length);
        show_length = (TextView) findViewById(R.id.show_length);
        go_walk = (Button) findViewById(R.id.walk);
        go_make = (Button) findViewById(R.id.make);
    }

    /**
     * Weight information
     */
    private ScaleRulerView.OnValueChangeListener input_weight_listener = new ScaleRulerView.OnValueChangeListener() {
        @Override
        public void onValueChange(float value) {
            show_weight.setText((int)value+getString(R.string.kg));
            weight = (int) value;
            weight_str = (int)value+getString(R.string.kg);
        }
    };
    /**
     * Get step size information
     */
    private ScaleRulerView.OnValueChangeListener input_length_listener = new ScaleRulerView.OnValueChangeListener() {
        @Override
        public void onValueChange(float value) {
            show_length.setText((int)value+getString(R.string.cm));
            length = (int) value;
            length_str = (int)value+getString(R.string.cm);
        }
    };
    /**
     * hide selector and keybroad
     */
    private View.OnTouchListener messageListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP){
                hideOthers();
            }
            return true;
        }
    };
    /**
     * init control Monitor
     */
    @Override
    protected void setViewsListener() {

        //======================================= Personal information 1/2 =======================================

        group.setOnCheckedChangeListener(this);
        input_birthday.setOnClickListener(this);
        input_height.setOnClickListener(this);
        next_action.setOnClickListener(this);
        dateViewHelper.setOnResultMessageListener(this);
        input_nick.setOnFocusChangeListener(this);
        input_birthday.setOnFocusChangeListener(this);
        input_height.setOnFocusChangeListener(this);
        height_picker.setOnSelectListener(this);
        personal_information_page_one.setOnTouchListener(messageListener);

        //======================================= Personal information 2/2 =======================================

        input_weight.setValueChangeListener(input_weight_listener);
        input_length.setValueChangeListener(input_length_listener);
        go_walk.setOnClickListener(this);
        go_make.setOnClickListener(this);
    }
    /**
     * Set related functions
     */
    @Override
    protected void setViewsFunction() {

        //======================================= Personal information 1/2 =======================================

        personal_information_page_one.setVisibility(View.VISIBLE);
        input_nick.setClickable(true);
        input_birthday.setClickable(true);
        input_height.setClickable(true);
        input_nick.setFocusableInTouchMode(true);
        input_birthday.setFocusableInTouchMode(true);
        input_height.setFocusableInTouchMode(true);
        //date selector
        choose_date.addView(dateViewHelper.getDataPick(inflater));
        //height seleector
        height_picker.setData(height_list);

        //======================================= personal info2/2 =======================================

        personal_information_page_two.setVisibility(View.GONE);
        ///The default is 50 kg, the minimum is 30 kg, the maximum is 130 kg --> unit kilogram
        input_weight.initViewParam(50, 130, 30);
        //Default 70 cm, minimum 40 cm, maximum 100 cm --> unit centimeters
        input_length.initViewParam(70, 100, 40);
    }

    /**
     * choose one
     * @param group
     * @param checkedId
     */
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        hideOthers();
        //sex
        switch (checkedId){
            case R.id.boy://boy
                gender_str = getResources().getString(R.string.boy);
                break;
            case R.id.girl://girl
                gender_str = getResources().getString(R.string.girl);
                break;
            default:
                gender_str = getResources().getString(R.string.boy);
                break;
        }
    }

    /**
     * hiden
     */
    private void hideOthers(){
        if (closeDataPicker == true){
            openPickerOrClose(false);

        }
        if (closeHeightPicker == true){
            openHeightPickerOrClose(false);

        }
        if (!nextShow){
            showNextBtn();
        }
        try{
            hideKeyBoard();
        } catch (Exception e)
        {
            Log.d("keybroad","null");
        }

    }
    /**
     * click event
     * @param v
     */
    @Override
    public void onClick(View v) {
        //======================================= Personal information 1/2 =======================================
        switch (v.getId()){
            case R.id.input_birthday://birthday
                openPickerOrClose(!closeDataPicker);
                if (!nextShow){
                    showNextBtn();
                }
                break;
            case R.id.input_height://height
                openHeightPickerOrClose(!closeHeightPicker);
                if (!nextShow){
                    showNextBtn();
                }
                break;
            case R.id.next://get  more information
                nick_str = input_nick.getText().toString();
                birthday_str = input_birthday.getText().toString();
                height_str = input_height.getText().toString();
                Log.e("information","性："+gender_str+"\t\t"+"nick name："+nick_str+"\t\t"+"Birthday："+birthday_str+"\t\theight："+height_str+"\t\tage："+custom_age+"age");
                if (!"".equals(nick_str) && !getString(R.string.please_write_birthday).equals(birthday_str) && !getString(R.string.please_write_height).equals(height_str)){
                    //save data
                    saveMessageOne();
                    //title personal information 1/2
                    // Hide personal information 2/2
                    //1. Display Personal Information 2
                    showAnimation(personal_information_page_one, R.anim.alpha_out);
                    personal_information_page_two.setVisibility(View.VISIBLE);
                    showAnimation(personal_information_page_two, R.anim.push_left_in).setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                        }
                        @Override
                        public void onAnimationEnd(Animation animation) {
                            //1.Display personal information 1
                            personal_information_page_one.setVisibility(View.GONE);
                            back = setTitleLeft(getString(R.string.personal_information_two));
                            //Set the displayed image
                            back.setImageResource(R.mipmap.all_back_black);
                            //Set back to the previous page
                            back.setOnClickListener(MainActivity.this);
                            Toast.makeText(MainActivity.this, getString(R.string.back_black_image), Toast.LENGTH_SHORT).show();
                        }
                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });
                }else {
                    Toast.makeText(this,getString(R.string.welcome_not_complate), Toast.LENGTH_SHORT).show();
                }
                break;
            //======================================= Personal information 2/2 =======================================
            case R.id.left_btn://Return to the previous step
                personal_information_page_two.setVisibility(View.GONE);
                showAnimation(personal_information_page_two, R.anim.push_left_out);
                personal_information_page_one.setVisibility(View.VISIBLE);
                showAnimation(personal_information_page_one, R.anim.wave_scale).setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        setTitle(getString(R.string.personal_information_one));
                    }
                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                Toast.makeText(this,getString(R.string.go_back), Toast.LENGTH_SHORT).show();
                break;
            case R.id.walk://Go shopping first
                saveMessageTwo();
                Toast.makeText(this,getString(R.string.take_a_look), Toast.LENGTH_SHORT).show();
                FunctionActivity.take_a_look = true;
                Intent functionIntent = new Intent(this, FunctionActivity.class);
                startActivity(functionIntent);
                finish();
                break;
            case R.id.make://Make plans
                saveMessageTwo();
                Toast.makeText(this,getString(R.string.set_plan), Toast.LENGTH_SHORT).show();
                Intent PlanningIntent = new Intent(this,PlanningActivity.class);
                startActivity(PlanningIntent);
                finish();
                break;
            default:
                break;
        }
    }

    /**
     * save first part data
     * */
    private void saveMessageOne(){
        SaveKeyValues.putStringValues("gender", gender_str);//sex
        SaveKeyValues.putStringValues("nick",nick_str);//nick name
        SaveKeyValues.putStringValues("birthday",birthday_str);//Birthday date
        SaveKeyValues.putIntValues("birth_year" ,year);
        SaveKeyValues.putIntValues("birth_month",month);
        SaveKeyValues.putIntValues("birth_day",day);
        SaveKeyValues.putStringValues("height_str",height_str);//Height with unit text
        int heightValue = Integer.parseInt(height_str.substring(0,height_str.length()-2));
        SaveKeyValues.putIntValues("height", heightValue);//Height value
        Log.d("Sace Heigh",SaveKeyValues.getIntValues("height",1000000)+"");
        SaveKeyValues.putIntValues("age",custom_age);//sex
    }
    /**
     * save Second part of the data
     */
    private void saveMessageTwo(){
        SaveKeyValues.putIntValues("weight",weight);//Weight value
        SaveKeyValues.putStringValues("weight_str", weight_str);//Weight information
        SaveKeyValues.putIntValues("length", length);//Step length value
        SaveKeyValues.putStringValues( "length_str", length_str);//Step length value
        SaveKeyValues.putIntValues("count",1);//Used to judge that it is not the first time to start
    }
    /**
     * Calendar information
     * @param map
     */
    @Override
    public void getMessage(Map<String, Object> map) {
        year = (int) map.get("year");
        month = (int) map.get("month");
        day = (int) map.get("day");
        custom_age = Integer.parseInt(map.get("age").toString());
        input_birthday.setText(year + getString(R.string.year) + month + getString(R.string.month) + day + getString(R.string.day));
    }

    /**
     * monitor
     * @param v
     * @param hasFocus
     */
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()){
            case R.id.input_nick:
                if (!nextShow){
                    showNextBtn();
                }
                break;
            case R.id.input_birthday:
                openPickerOrClose(hasFocus);
                break;
            case R.id.input_height:
                openHeightPickerOrClose(hasFocus);
                break;
            default:
                break;
        }
    }

    /**
     * Show or hide the date selector
     * @param flag
     */
    private void openPickerOrClose(boolean flag){
        if (flag){
            Log.e(TAG, "拎到生日");
            hideKeyBoard();
            choose_date.setVisibility(View.VISIBLE);
            hideNextBtn();
            showAnimation(choose_date, R.anim.push_up_in);
            closeDataPicker = true;
        }else {
            if (closeDataPicker == true){
                showAnimation(choose_date, R.anim.push_up_out);
                choose_date.setVisibility(View.GONE);
                showNextBtn();
                closeDataPicker = false;
            }
        }
    }
    /**
     * Show or hide height selector
     * @param flag
     */
    private void openHeightPickerOrClose(boolean flag){
        if (flag){
            Log.e(TAG, "拎到身高");
            hideKeyBoard();
            choose_height.setVisibility(View.VISIBLE);
            hideNextBtn();
            showAnimation(choose_height, R.anim.push_left_in);
            closeHeightPicker = true;
        }else {
            if (closeHeightPicker == true){
                showAnimation(choose_height,R.anim.push_left_out);
                choose_height.setVisibility(View.GONE);
                showNextBtn();
                closeHeightPicker = false;
            }
        }
    }
    /**
     * show next step button
     */
    private void showNextBtn(){
        if (closeDataPicker == false && closeHeightPicker== false){
            next_action.setVisibility(View.VISIBLE);
            showAnimation(next_action, R.anim.fade_in);
            nextShow = true;
        }
    }

    /**
     * hide next step button
     */
    private void hideNextBtn(){
        showAnimation(next_action, R.anim.fade_out);
        next_action.setVisibility(View.INVISIBLE);
        nextShow = false;
    }

    /**
     * animal
     * @param view
     * @param animID
     */
    private Animation showAnimation(View view, int animID){
        Animation animation = AnimationUtils.loadAnimation(this,animID);
        view.setAnimation(animation);
        animation.start();
        return animation;
    }

    /**
     * height date
     * @param text
     */
    @Override
    public void onSelect(String text) {
        input_height.setText(text + getString(R.string.cm));
    }

    /**
     * hide keybroad
     */
    private void  hideKeyBoard(){
       ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(MainActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /**
     * hide back
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            return false;
        }
        return false;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // TODO Auto-generated method stub
        super.onConfigurationChanged(newConfig);

    }
}
